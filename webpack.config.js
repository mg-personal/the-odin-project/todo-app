const path = require('path');

module.exports = {
  entry: ['./src/index.js', '/Users/maurogarcia/the_odin_project/todo-app/dist/style.css'],
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
};