function homeRender() {
    var bodyContent = document.querySelector('.content');


    var itemsDiv = document.createElement('div');
    var menuDiv = document.createElement('div');
    var todoDiv = document.createElement('div');
    todoDiv.setAttribute('class', 'todo-div');
    itemsDiv.appendChild(todoDiv);
    var titleDiv = document.createElement('div');
    var listsDiv = document.createElement('div');
    var addNewDiv = document.createElement('button');

    bodyContent.appendChild(itemsDiv);
    itemsDiv.appendChild(addNewDiv);
    bodyContent.appendChild(menuDiv);
    menuDiv.appendChild(titleDiv);
    menuDiv.appendChild(listsDiv);

    itemsDiv.setAttribute('class', 'items-div');
    menuDiv.setAttribute('class', 'menu-div');
    titleDiv.setAttribute('id', 'title-div');
    listsDiv.setAttribute('id', 'lists-div');
    addNewDiv.setAttribute('id', 'add-new-div');
    addNewDiv.innerHTML = '+';

    let allProjects = [];
    let firstProjects = [];
    let projects = [];

    titleDiv.innerHTML = "TITLE";

    class todoProject {
        constructor(todoProjectName, todoProjectItem) {
            this.todoProjectName = todoProjectName;
            this.todoProjectItem = todoProjectItem;
        }
    }

    class projectName {
        constructor(projectTitle) {
            this.projectTitle = projectTitle;
        }
    }
    class todoItem {
        constructor(description) {
            // this.title = title;
            // this.projectName = projectName;
            this.description = description;
        }
    }

    const mauroItem = new todoItem('Description1');
    const camilaItem = new todoItem('Description2');
    const julianItem = new todoItem('Description3');
    const gastonItem = new todoItem('Description4');

    const mauro1Item = new projectName('projectX');
    const camila1Item = new projectName('projectY');

    const fake1 = new todoProject(mauro1Item, mauroItem);
    const fake2 = new todoProject(camila1Item, camilaItem);
    const fake3 = new todoProject(mauro1Item, julianItem);

    projects.push(mauroItem);
    projects.push(camilaItem);
    firstProjects.push(julianItem);
    firstProjects.push(gastonItem);

    // allProjects.push(firstProjects, projects);
    allProjects.push(fake1);
    allProjects.push(fake2);
    allProjects.push(fake3);

    // test with multiplae objects.
    // solve the multiple project name rendering
    // maybe try to delete duplicated text!

    // listsDiv.innerHTML = Object.keys(allProjects);

    // for (var i = 0; i < allProjects.length; i++) {
    //     console.log(allProjects[i])
    //     for (var key in allProjects[i]) {           
    //         // if (allProjects[i][key].projectName === 'project2') {
    //         //     console.log(allProjects[i][key].description);
    //         // }
    //         // console.log(allProjects[i][key].projectName)
    //         var projectTitleName = document.createElement('h1');

    //         listsDiv.appendChild(projectTitleName);
    //         projectTitleName.innerHTML = allProjects[i][key].projectName;
    //     }
    // }

    // console.log(allProjects);

    for (var i = 0; i < allProjects.length; i++) {
        // console.log(allProjects[i].todoProjectName.projectTitle);
        var projectTitleName = document.createElement('h1');
        listsDiv.appendChild(projectTitleName);
        projectTitleName.innerHTML = allProjects[i].todoProjectName.projectTitle;
        if (allProjects[i].todoProjectName.projectTitle === 'projectX') {
            console.log(allProjects[i].todoProjectItem.description);
        }
        // for (var key in allProjects[i]) {
        //     if (allProjects[i][key].projectTitle !== undefined) {
        //         console.log(allProjects[i][key].projectTitle);
        //     }
        //     // console.log(allProjects[i][key]);
        //     // if (allProjects[i][key].projectNameSelected === 'projectX') {
        //     //     console.log(allProjects[i]);
        //     // }
        // }
    }



    function renderItems() {
        for (let i = 0; i < projects.length; i++) {

            var itemList = document.createElement('ul');
            // var itemTitle = document.createElement('li');
            var itemDescription = document.createElement('li');
            // var itemTitleLink = document.createElement('a');
            var itemDescriptionLink = document.createElement('a');


            todoDiv.appendChild(itemList);
            // itemList.appendChild(itemTitle);
            itemList.appendChild(itemDescription);
            // itemTitle.appendChild(itemTitleLink);
            itemDescription.appendChild(itemDescriptionLink);



            itemList.setAttribute('id', 'todo-list');
            // itemTitle.setAttribute('id', 'todo-item');
            itemDescription.setAttribute('id', 'todo-item');

            // itemTitleLink.innerHTML = `${projects[i].title}`;
            itemDescriptionLink.innerHTML = `${projects[i].description}`;

            // itemList.addEventListener('click', () => {
            //     console.log(`${projects[i].description}`);
            // })
        };
        deleteItemSelected();
    };

    function deleteItemSelected() {
        var todoItemSelected = document.querySelectorAll('#todo-item');

        // todoItemSelected.addEventListener('click', () => {
        //     // for( var i = 0; i < projects.length; i++){ 
        //     //     if ( projects[i].description === `${projects[i].description}`) {
        //     //       projects.splice(i, 1); 
        //     //     }
        //     //  }
        //     console.log(`${todoItemSelected.value}`);
        //     console.log('DELETED');
        //     //  renderItems();
        // });

        todoItemSelected.forEach(function (elem) {
            elem.addEventListener('click', () => {
                console.log(elem.innerText);
                // elem.setAttribute('class', 'completed');
                setTodoStatus(elem);
                // var itemText = elem.innerText;
                // var divToErase = document.querySelector('.todo-div');

                // for (var i = 0; i < projects.length; i++) {
                //     if (projects[i].description === itemText) {
                //         projects.splice(i, 1);
                //     }
                // }

                // while (divToErase.hasChildNodes()) {
                //     divToErase.removeChild(divToErase.firstChild);
                // };

                // renderItems();
            })
        })
    }

    function setTodoStatus(elem) {
        if (elem.className === 'completed') {
            elem.setAttribute('class', 'not-completed');
        } else {
            elem.setAttribute('class', 'completed');
        }
    }

    renderItems();




    var newTodoFormContainer = document.createElement('div');
    var newTodoForm = document.createElement('div');
    var newTodo = document.createElement('input');
    var newTodoButton = document.createElement('button');

    itemsDiv.appendChild(newTodoFormContainer);
    newTodoFormContainer.appendChild(newTodoForm);
    newTodoForm.appendChild(newTodo);
    newTodoForm.appendChild(newTodoButton);


    newTodoFormContainer.setAttribute('class', 'new-todo-form-container');
    newTodoForm.setAttribute('class', 'new-todo-form');
    newTodo.setAttribute('id', 'new-todo-input');
    newTodoButton.setAttribute('id', 'new-todo-button');

    newTodoButton.innerHTML = "Add";
    newTodoFormContainer.style.display = "none";


    addNewDiv.addEventListener('click', () => {
        newTodoFormContainer.style.display = "block";
    })

    function addNewTodo() {
        var newTodoValue = newTodo.value;
        const newItem = new todoItem(newTodoValue);
        projects.push(newItem);
        var divToErase = document.querySelector('.todo-div');
        while (divToErase.hasChildNodes()) {
            divToErase.removeChild(divToErase.firstChild);
        };
        renderItems();
        newTodo.value = '';
        newTodoFormContainer.style.display = "none";
    }

    newTodoButton.addEventListener('click', () => {
        addNewTodo();
    })



};

export { homeRender }